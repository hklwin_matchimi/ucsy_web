class CreateAuthentications < ActiveRecord::Migration
  def change
    create_table :authentications do |t|
			t.column :user_id, :string
			t.column :authentication_code, :string
			t.column :issued_at, :datetime
    end
  end
end

