class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

	t.column :email, :string
	t.column :fullname, :string
	t.column :nickname, :string
	t.column :password, :string
	t.column :gender_id, :int, :default => 1
	t.column :dob, :date, :default => '1987-01-01'
	t.column :profile_pic, :string
	t.column :status, :text
	t.column :is_alumnus, :boolean, :default => false
	t.column :batch_no, :int, :default => nil
	t.column :last_seen_at, :datetime
	t.column :is_admin, :boolean, :default => false

      t.timestamps
    end
  end
end
