class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|

	t.column :user_id, :int
	t.column :received_date, :date
	t.column :amount, :decimal
	t.column :currency_id, :int
	t.column :remark, :text
	t.column :admin_id, :int

      t.timestamps
    end
	change_column :donations, :id , "bigint NOT NULL AUTO_INCREMENT"
	execute "ALTER TABLE `donations`
        	ADD CONSTRAINT `fk_donations_users`
        	FOREIGN KEY (`user_id`)
        	REFERENCES `users`(`id`),
		ADD CONSTRAINT `fk_donations_admins`
		FOREIGN KEY (`admin_id`)
		REFERENCES `users`(`id`),
		ADD CONSTRAINT `fk_donations_currencies`
		FOREIGN KEY (`currency_id`)
		REFERENCES `currencies`(`id`),
		ADD INDEX `fk_donations_users_idx`(`user_id`),
		ADD INDEX `fk_donations_admins_idx`(`admin_id`),
		ADD INDEX `fk_donations_currencies_idx`(`currency_id`)"
  end
end
