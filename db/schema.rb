# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140927170022) do

  create_table "authentications", force: true do |t|
    t.string   "user_id"
    t.string   "authentication_code"
    t.datetime "issued_at"
  end

  create_table "currencies", force: true do |t|
    t.string "currency"
  end

  create_table "donations", force: true do |t|
    t.integer  "user_id"
    t.date     "received_date"
    t.decimal  "amount",        precision: 10, scale: 0
    t.integer  "currency_id"
    t.text     "remark"
    t.integer  "admin_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "donations", ["admin_id"], name: "fk_donations_admins_idx", using: :btree
  add_index "donations", ["currency_id"], name: "fk_donations_currencies_idx", using: :btree
  add_index "donations", ["user_id"], name: "fk_donations_users_idx", using: :btree

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "fullname"
    t.string   "nickname"
    t.string   "password"
    t.integer  "gender_id",      default: 1
    t.date     "dob",            default: '1987-01-01'
    t.string   "profile_pic"
    t.text     "status"
    t.boolean  "is_alumnus",     default: false
    t.integer  "batch_no"
    t.datetime "last_seen_at"
    t.boolean  "is_admin",       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone_no"
    t.string   "member_no"
    t.boolean  "is_super_admin", default: false
  end

end
