class UserMailer < ActionMailer::Base

	default :from => "UCSY 15th Batch Charity Foundation <ucsycf@gmail.com>"

  def welcome_system_email(fullname, email, value, superuser_emails)
		logger.debug "Sending out welcome email"

		begin
			@fullname = fullname
			@email = email
			@password = value
		
			mail(:to => email, :subject => "Welcome to UCSY 15th Batch Charity Foundation", :cc => superuser_emails)
		rescue
			logger.debug "UserMailer Exception: #{$!}"
			logger.debug "UserMailer CallStack: #{$@}"
			logger.info "There was an error while sending out email."
		end
  end

	def reset_password_email(fullname, email, value)
		logger.debug "Sending out reset password email"

		begin
			@fullname = fullname
			@email = email
			@password = value

			mail(:to => email, :subject => "Reset password request")
		rescue
			logger.debug "UserMailer Exception: #{$!}"
			logger.debug "UserMailer CallStack: #{$@}"
			logger.info "There was an error while sending out email."
		end
	end

	def new_donation(donation, superuser_emails)
		logger.debug "Sending out new donation email"

		begin
			@donation = donation
			superuser_emails << donation.admin.email

			mail(:to => donation.user.email, :subject => "New donation", :cc => superuser_emails)
		rescue
			logger.debug "UserMailer Exception: #{$!}"
			logger.debug "UserMailer CallStack: #{$@}"
			logger.info "There was an error while sending out email."
		end
	end

	def test_email()
			email = 'hklwin.sg@gmail.com'
			mail(:to => email, :subject => "Welcome to UCSY 15th Batch Charity Foundation", :body => "Gmail is stupid")
	end
end
