class HomeController < ApplicationController
	def index
		#@batches = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27]
		user_id = session["user_id"]

		if user_id
			redirect_to "/users/dashboard" and return
		end
	end

	def request_reset_password

	end
end
