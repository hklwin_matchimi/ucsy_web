class DonationsController < ApplicationController
	def new
		@currencies = []
		@donations = []
		begin
			session_user_id = session["user_id"]
			session_user = User.find_by_id(session_user_id)

			if session_user
				if session_user.authentication
					if ((DateTime.now() - session_user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
						session_user.authentication.issued_at = DateTime.now()
						session_user.authentication.save()

						@currencies = Currency.all
						@donations = Donation.find_by_sql(["select d.*, a.fullname as received_by, u.fullname from donations d inner join users a on a.id = d.admin_id inner join users u on u.id = d.user_id where d.created_at >= now() - INTERVAL 1 DAY"])
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
					end
				else
					flash["alert"] = "Your session is expired. Please try login again."
					flash["type"] = "warning"

					reset_session
				end
			else
				reset_session
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"
		end
	end

	def edit
		@currencies = []
		@donations = []
		donation_id = params["id"]
		begin
			session_user_id = session["user_id"]
			session_user = User.find_by_id(session_user_id)

			if session_user
				if session_user.authentication
					if ((DateTime.now() - session_user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
						session_user.authentication.issued_at = DateTime.now()
						session_user.authentication.save()

						@currencies = Currency.all
						@donation = Donation.find_by_id(donation_id)
						@donations = Donation.find_by_sql(["select d.*, a.fullname as received_by, u.fullname from donations d inner join users a on a.id = d.admin_id inner join users u on u.id = d.user_id where d.created_at >= now() - INTERVAL 1 DAY"])
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
					end
				else
					flash["alert"] = "Your session is expired. Please try login again."
					flash["type"] = "warning"

					reset_session
				end
			else
				reset_session
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"
		end
	end

	def delete
		response_data = {}
		response_data["result"] = "failed"

		begin
			session_user_id = session["user_id"]
			session_user = User.find_by_id(session_user_id)

			if session_user
				if session_user.authentication
					if ((DateTime.now() - session_user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
						session_user.authentication.issued_at = DateTime.now()
						session_user.authentication.save()

						donation_id = params[:donation_id]
						donation = Donation.find_by_id(donation_id)
						
						if donation
							logger.info "Deleting donation"
							logger.info "id=#{donation.id}"
							logger.info "user_id=#{donation.user_id}"
							logger.info "received_date=#{donation.received_date}"
							logger.info "amount=#{donation.amount}"
							logger.info "currency_id=#{donation.currency_id}"
							logger.info "remark=#{donation.remark}"
							logger.info "admin_id=#{donation.admin_id}"
							logger.info "created_at=#{donation.created_at}"
							logger.info "updated_at=#{donation.updated_at}"
							logger.info "deleted_by=#{request.session['user_id']}"

							donation.delete()

							response_data["result"] = "successful"
						else
							response_data["message"] = "Donation not found"
						end
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
					end
				else
					flash["alert"] = "Your session is expired. Please try login again."
					flash["type"] = "warning"

					reset_session
				end
			else
				reset_session
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"

			response_data["result"] = "failed"
			response_data["message"] = "Error"
		end

		render :json => response_data and return
	end

	def edit_donation
		ActiveRecord::Base.transaction do
			donation_id = ""
			begin
				session_user_id = session["user_id"]
				session_user = User.find_by_id(session_user_id)

				if session_user
					if session_user.authentication
						if ((DateTime.now() - session_user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
							session_user.authentication.issued_at = DateTime.now()
							session_user.authentication.save()

							logger.debug "#{params.to_json}"

							donation_id = params[:donation_id]
							amount = params[:amount]
							currency_id = params[:currency]
							received_date = params[:received_date]
							remark = params[:remark]

							donation = Donation.find_by_id(donation_id)

							if donation

								logger.info "Editing donation"
								logger.info "id=#{donation.id}"
								logger.info "user_id=#{donation.user_id}"
								logger.info "received_date=#{donation.received_date}"
								logger.info "amount=#{donation.amount}"
								logger.info "currency_id=#{donation.currency_id}"
								logger.info "remark=#{donation.remark}"
								logger.info "admin_id=#{donation.admin_id}"
								logger.info "created_at=#{donation.created_at}"
								logger.info "updated_at=#{donation.updated_at}"
								logger.info "edited_by=#{request.session['user_id']}"

								donation.amount = amount
								donation.currency_id = currency_id
								donation.received_date = received_date
								donation.remark = remark

								donation.save()

								flash["alert"] = "Donation has been edited."
								flash["type"] = "success"

								redirect_to "/donations/edit/#{donation_id}" and return
							else
								flash["alert"] = "Donation not found."
								flash["type"] = "warning"

								redirect_to donations_search_path and return
							end

						else
							flash["alert"] = "Your session is expired. Please try login again."
							flash["type"] = "warning"

							reset_session
						end
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
					end
				else
					reset_session
				end
			rescue
				logger.error "#{$!}"
				logger.debug "#{$@}"

				flash["alert"] = "Sorry, cannot edit donation."
				flash["type"] = "warning"				
				ActiveRecord::Rollback
			end
		end

		redirect_to donations_search_path and return
	end

	def create_donation
		ActiveRecord::Base.transaction do
			begin
				session_user_id = session["user_id"]
				session_user = User.find_by_id(session_user_id)

				if session_user
					if session_user.authentication
						if ((DateTime.now() - session_user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
							session_user.authentication.issued_at = DateTime.now()
							session_user.authentication.save()

							logger.debug "#{params.to_json}"

							selected = params[:selected]
							admin_id = session["user_id"]
							user_id = params[:user_id]
							amount = params[:amount]
							currency_id = params[:currency]
							received_date = params[:received_date]
							remark = params[:remark]

							if selected == 'true'
								Donation.create(:user_id => user_id,
									:received_date => received_date,
									:amount => amount,
									:currency_id => currency_id,
									:remark => remark,
									:admin_id => admin_id)

								flash["alert"] = "New donation has been submitted."
								flash["type"] = "success"

								donation = Donation.last

								superuser_emails = User.where(['is_super_admin=1']).pluck(:email)
								admin_id = session["user_id"]
								admin = User.find_by_id(admin_id)
								superuser_emails << admin.email

								UserMailer.new_donation(donation, superuser_emails).deliver
							else
								flash["alert"] = "Failed to submit new donation. Please check your data."
								flash["type"] = "warning"
							end

						else
							flash["alert"] = "Your session is expired. Please try login again."
							flash["type"] = "warning"

							reset_session
						end
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
					end
				else
					reset_session
				end
				
			rescue
				logger.error "#{$!}"
				logger.debug "#{$@}"

				flash["alert"] = "Sorry, cannot submit donation."
				flash["type"] = "warning"				
				ActiveRecord::Rollback
			end
		end

		redirect_to donations_new_path and return
	end

	def search
		@donations = []

		begin
			session_user_id = session["user_id"]
			session_user = User.find_by_id(session_user_id)

			if session_user
				if session_user.authentication
					if ((DateTime.now() - session_user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
						session_user.authentication.issued_at = DateTime.now()
						session_user.authentication.save()

						@donations = Donation.all

					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
					end
				else
					flash["alert"] = "Your session is expired. Please try login again."
					flash["type"] = "warning"

					reset_session
				end
			else
				reset_session
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"
		end
	end

  def export
    @donations = []
    begin
      session_user_id = session["user_id"]
      session_user = User.find_by_id(session_user_id)

      if session_user
        if session_user.authentication
          if ((DateTime.now() - session_user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
            session_user.authentication.issued_at = DateTime.now()
            session_user.authentication.save()

						currencyquery = Currency.select('id,currency as curr').to_sql
						userquery = User.select('id, fullname, nickname').to_sql
						@donations = Donation.joins("INNER JOIN (#{userquery}) users ON donations.user_id = users.id").joins("INNER JOIN (#{currencyquery}) currencies ON donations.currency_id = currencies.id")
														 .select('donations.id, users.fullname, users.nickname, donations.received_date,donations.amount, currencies.curr, donations.remark').order(:id)

						donation_csv = CSV.generate do |csv|
							# header row
							csv << ["Trans. No.","Donor Name", "Nick Name", "Amount", "Currency", "Received Date", "Remark"]
							# data rows
							@donations.each do |donation|
								csv << [donation.id, donation.fullname, donation.nickname, donation.amount, donation.curr, donation.received_date, donation.remark]
							end
						end
						send_data(donation_csv, :type => 'text/csv', :filename => 'donations.csv')

						#respond_to do |format|
            #  format.csv { send_data @donations.export_csv }
             # #format.xls # { send_data @products.to_csv(col_sep: "\t") }
						#end

          else
            redirect_to "/" and return
          end
        else
          redirect_to "/" and return
        end
      else
        redirect_to "/" and return
      end
    rescue
      logger.error "#{$!}"
      logger.debug "#{$@}"
    end
  end

end
