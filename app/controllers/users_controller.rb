require 'digest'
require 'mailer'
require 'user_mailer'
require 'digest/md5'

class UsersController < ApplicationController

	def change_admin
		begin
			response_data = {}
			response_data["result"] = "failed"
			response_data["is_admin"] = false
	
			session_user_id = session["user_id"]
			session_user = User.find_by_id(session_user_id)

			if session_user
				if session_user.authentication
					if ((DateTime.now() - session_user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
						session_user.authentication.issued_at = DateTime.now()
						session_user.authentication.save()
						
						user_id = params[:user_id]

						logger.debug "#{params}"
						logger.debug "UserId = #{user_id}"

						user = User.find_by_id(user_id)
						
						if user
							if user.is_admin
								user.is_admin = false
								user.save()
								response_data["is_admin"] = false
							else
								user.is_admin = true
								user.save()
								response_data["is_admin"] = true
							end

							response_data["result"] = "successful"
						else
							response_data["message"] = "User not found"
						end
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
						redirect_to root_path and return
					end
				else
					flash["alert"] = "Your session is expired. Please try login again."
					flash["type"] = "warning"

					reset_session
					redirect_to root_path and return
				end
			else
				reset_session
				redirect_to root_path and return
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"

			response_data["message"] = "Error"
		end

		render :json => response_data and return
	end

	def reset_password
		begin
			email = params[:forgot_email]

			user = User.where(['email = ?', email]).first

			if user
				obj = [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten
				value  =  (0...6).map{ obj[rand(obj.length)] }.join
				password = Digest::MD5.hexdigest(value)

				user.password = password
				user.save()

				UserMailer.reset_password_email(user.fullname, email, value).deliver

				flash["alert"] = "Password has been successfully reset. Please check your email to receive new password."
				flash["type"] = "success"
			else
				flash["alert"] = "Email address not found."
				flash["type"] = "warning"

				redirect_to request_reset_password_path and return
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"

			flash["alert"] = "Fail to reset password."
			flash["type"] = "warning"

			ActiveRecord::Rollback

			redirect_to request_reset_password_path and return
		end
		redirect_to root_path and return
	end

	def change_password
		
	end

	def change_password_action
		begin
			old_password = Digest::MD5.hexdigest(params[:old_password])
			new_password = Digest::MD5.hexdigest(params[:new_password])

			user_id = session["user_id"]
			@user = User.find_by_id(user_id)

			if @user
				if @user.authentication
					if ((DateTime.now() - @user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
						@user.authentication.issued_at = DateTime.now()
						@user.authentication.save()

						if @user.password == old_password
							@user.password = new_password
							@user.save()
							flash["alert"] = "Password is successfully changed."
							flash["type"] = "info"

							redirect_to users_dashboard_path and return
						else
							flash["alert"] = "Incorrect old password."
							flash["type"] = "warning"
						end
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
						redirect_to root_path and return
					end
				else
					flash["alert"] = "Your session is expired. Please try login again."
					flash["type"] = "warning"

					reset_session
					redirect_to root_path and return
				end
			else
				flash["alert"] = "Invalid user."
				flash["type"] = "warning"
				
				reset_session
				redirect_to root_path and return
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"

			flash["alert"] = "Fail to change password."
			flash["type"] = "warning"

			ActiveRecord::Rollback
		end
		redirect_to users_change_password_path and return
	end

	def upload_profile_pic
		begin
			user_id = session["user_id"]
			@user = User.find_by_id(user_id)

			unless @user
				logger.debug "Invalid User"

				flash["alert"] = "Invalid User."
				flash["type"] = "warning"

				redirect_to users_dashboard_path and return
			end

			if params[:file] == nil
				logger.debug "Null File"

				flash["alert"] = "Null file detected."
				flash["type"] = "warning"

				redirect_to users_dashboard_path and return
			end

			logger.debug "FileName = #{params[:file].original_filename}"
			logger.debug "FileSize = #{params[:file].tempfile.size}"

			if params[:file].tempfile.size == 0
				logger.debug "Blank File"

				flash["alert"] = "Blank file detected."
				flash["type"] = "warning"

				redirect_to users_dashboard_path and return
			end

			unless params[:file] && (tmpfile = params[:file].tempfile) && (name = params[:file].original_filename)
				@error = "No File"

				flash["alert"] = "No file selected."
				flash["type"] = "warning"

				redirect_to users_dashboard_path and return
			end

			logger.debug "Uploading file - #{name}"

			directory = "#{Rails.root}/public/profile_pic"
			timestamp = Time.now.to_i
			file_name = timestamp.to_s() + "_" + name

			logger.debug "New FileName = #{file_name}"

			path = File.join(directory, file_name)
			file = File.open(path, "wb")

			while blk = tmpfile.read(65536)
				# here you would write it to its final location
				file.write(blk)
			end

			file.close

			@user.profile_pic = "/profile_pic/#{file_name}"
			@user.save()

			flash["alert"] = "Profile picture is successfully changed."
			flash["type"] = "info"

		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"

			flash["alert"] = "Fail to change profile picture."
			flash["type"] = "warning"

			ActiveRecord::Rollback
		end

		redirect_to users_dashboard_path and return
	end

	def sign_in
		begin
			email = params[:email]
			password = Digest::MD5.hexdigest(params[:password])

			user = User.where(["email = ? and password = ?", email, password]).first
			#.find(:email => email, :password => password)

			if user
				session["user_id"] = user.id
				session["member_no"] = user.member_no
				session["user_fullname"] = user.fullname
				session["user_nickname"] = user.nickname
				session["is_admin"] = user.is_admin
				session["is_super_admin"] = user.is_super_admin

				auth_code = 'No Code Required Right Now.'#AuthenticationHandler.create_authentication_code()
				
				if user.authentication
					user.authentication.authentication_code = auth_code
					user.authentication.issued_at = DateTime.now()
					user.authentication.save()
				else
					Authentication.create(:user_id => user.id,
					:authentication_code => auth_code,
					:issued_at => DateTime.now())
				end

				redirect_to "/users/dashboard" and return
			else
				flash["alert"] = "Invalid login."
				flash["type"] = "warning"
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"

			flash["alert"] = "Sorry, internal server error occurred. Please contact system admin."
			flash["type"] = "warning"
		end

		redirect_to root_path and return
	end

	def dashboard
		begin
			user_id = session["user_id"]
			@user = User.find_by_id(user_id)

			logger.debug "#{user_id}"
			
			if @user
				if @user.authentication
					logger.debug "#{DateTime.now()}"
					logger.debug "#{@user.authentication.issued_at.to_datetime()}"
					logger.debug "#{((DateTime.now() - @user.authentication.issued_at.to_datetime()) * 24 * 60).to_i}"
					
					if ((DateTime.now() - @user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
						@user.authentication.issued_at = DateTime.now()
						@user.authentication.save()

						@donations = Donation.where(["user_id = ?", user_id])

						logger.debug("#{@donations.length}")
					else
						logger.debug "auth err 1"

						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
						redirect_to root_path and return
					end
				else
					logger.debug "auth err 2"

					flash["alert"] = "Your session is expired. Please try login again."
					flash["type"] = "warning"

					reset_session
					redirect_to root_path and return
				end
			else
				logger.debug "auth err 3"

				reset_session
				redirect_to root_path and return
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"

			flash["alert"] = "Your session is expired. Please try login again."
			flash["type"] = "warning"

			reset_session
			redirect_to root_path and return
		end
	end

	def sign_out
		begin
			reset_session
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"

			flash["alert"] = "Sorry, internal server error occurred. Please contact system admin."
			flash["type"] = "warning"
		end
		redirect_to root_path and return
	end

	def profile
		begin
			user_id = params["id"]
			session_user_id = session["user_id"]

			@user = User.find_by_id(user_id)
			@session_user = User.find_by_id(session_user_id)

			if @session_user
				if @session_user.authentication
					if ((DateTime.now() - @session_user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
						@session_user.authentication.issued_at = DateTime.now()
						@session_user.authentication.save()
						
						if @session_user_id.to_i == user_id.to_i
							redirect_to users_dashboard_path and return
						end

						unless @user
							redirect_to root_path and return
						end

						#@donations = Donation.where(["user_id = ?", user_id])
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
						redirect_to root_path and return
					end
				else
					flash["alert"] = "Your session is expired. Please try login again."
					flash["type"] = "warning"

					reset_session
					redirect_to root_path and return
				end
			else
				reset_session
				redirect_to root_path and return
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"
			redirect_to root_path and return
		end
	end

	def new
		begin
			user_id = session["user_id"]
			@user = User.find_by_id(user_id)
			
			if @user
				if @user.authentication
					if ((DateTime.now() - @user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
						@user.authentication.issued_at = DateTime.now()
						@user.authentication.save()

						@users = User.where(["created_at >= now() - INTERVAL 1 DAY"])
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
						redirect_to root_path and return
					end
				else
					flash["alert"] = "Your session is expired. Please try login again."
					flash["type"] = "warning"

					reset_session
					redirect_to root_path and return
				end
			else
				reset_session
				redirect_to root_path and return
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"
		end
	end

	def search
		@users = []

		begin
			user_id = session["user_id"]
			@user = User.find_by_id(user_id)

			if @user
				if @user.authentication
					if ((DateTime.now() - @user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
						@user.authentication.issued_at = DateTime.now()
						@user.authentication.save()

						seed = params["seed"]
						if seed
							seed = '%' + seed[0..15] + '%'
							@users = User.where(["fullname like ? or nickname like ? or id like ?", seed, seed, seed])
						else
							@users = User.all
						end
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
						redirect_to root_path and return
					end
				else
					flash["alert"] = "Your session is expired. Please try login again."
					flash["type"] = "warning"

					reset_session
					redirect_to root_path and return
				end
			else
				reset_session
				redirect_to root_path and return
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"
		end
	end

	def find_users
		names = []
		ids = []
		resp = {}
		resp["names"] = names
		resp["ids"] = ids

		begin
			user_id = session["user_id"]
			@user = User.find_by_id(user_id)

			if @user
				if @user.authentication
					if ((DateTime.now() - @user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
						@user.authentication.issued_at = DateTime.now()
						@user.authentication.save()
						
						seed = params[:seed]
						if seed.length > 2
							seed = '%' + seed[0..20] + '%'
							users = User.where(["fullname like ? or nickname like ? or id like ?", seed, seed, seed])
							for user in users
								names << "#{user.id}, #{user.fullname} (#{user.nickname})"
								ids << user.id
							end
						end
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
						redirect_to root_path and return
					end
				else
					flash["alert"] = "Your session is expired. Please try login again."
					flash["type"] = "warning"

					reset_session
					redirect_to root_path and return
				end
			else
				reset_session
				redirect_to root_path and return
			end
		rescue
			logger.error "#{$!}"
			logger.debug "#{$@}"
		end

		render :json => resp and return
	end

	def update_profile
		ActiveRecord::Base.transaction do
			begin
				logger.debug "#{params.to_json}"

				user_id = params[:user_id]
				fullname = params[:fullname]
				nickname = params[:nickname]
				email = params[:email]
				phone_no = params[:phone_no]
				gender_id = params[:gender]
				dob = params[:dob]
				is_alumnus = [:alumnus]
				batch_no = params[:batch_no]
				
				user = User.find_by_id(user_id)
				if user
					if user.authentication
						if ((DateTime.now() - @user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
							user.authentication.issued_at = DateTime.now()
							user.authentication.save()
							
							user.fullname = fullname
							user.nickname = nickname
							user.email = email
							user.phone_no = phone_no
							user.gender_id = gender_id
							user.dob = dob
							user.is_alumnus = is_alumnus
							user.batch_no = batch_no

							user.save()

							flash["alert"] = "Your profile is updated."
							flash["type"] = "success"
						else
							flash["alert"] = "Your session is expired. Please try login again."
							flash["type"] = "warning"

							reset_session
							redirect_to root_path and return
						end
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
						redirect_to root_path and return
					end
				else
					reset_session
					redirect_to root_path and return
				end
			rescue
				logger.error "#{$!}"
				logger.debug "#{$@}"

				flash["alert"] = "Fail to update your profile."
				flash["type"] = "warning"

				ActiveRecord::Rollback
			end
		end

		redirect_to users_dashboard_path and return
	end

	def send_email
			begin
					logger.debug "send_email starts"

					UserMailer.test_email().deliver

					render :text => "Email was sent" and return
			rescue
					logger.debug "#{$!}"
					logger.debug "#{$@}"
					render :text => "#{$!}" and return
			end
			render :text => "Email was not sent" and return
	end

	def create_user
		ActiveRecord::Base.transaction do
			begin
				logger.debug "#{params.to_json}"

				session_user_id = session["user_id"]
				session_user = User.find_by_id(session_user_id)
				if session_user
					if session_user.authentication
						if ((DateTime.now() - session_user.authentication.issued_at.to_datetime()) * 24 * 60).to_i <= 15
							session_user.authentication.issued_at = DateTime.now()
							session_user.authentication.save()

							fullname = params[:fullname]
							member_no = params[:member_no]
							nickname = params[:nickname]
							email = params[:email]
							phone_no = params[:phone_no]
							# Password is not required. It is auto-generated by system.
							#password = Digest::MD5.hexdigest(params[:password])
							gender_id = params[:gender]
							dob = params[:dob]
							is_alumnus = params[:alumnus]
							batch_no = params[:batch_no]

							# Check if email alread exists
							user = User.where(["email = ?", email]).first
							if user
								# email already exists, return error message.
								flash["alert"] = "Email address already exists in system. Please choose another email"
								flash["type"] = "warning"
							else
								# email not found, proceed registration.
								obj = [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten
								value  =  (0...6).map{ obj[rand(obj.length)] }.join
								
								password = Digest::MD5.hexdigest(value)

								user = User.create(:fullname => fullname,
									:member_no => member_no,
									:nickname => nickname,
									:email => email,
									:phone_no => phone_no,
									:password => password,
									:gender_id => gender_id,
									:dob => dob,
									:is_alumnus => is_alumnus,
									:batch_no => batch_no)
								
								superuser_emails = User.where(['is_super_admin=1']).pluck(:email)
								admin_id = session["user_id"]
								admin = User.find_by_id(admin_id)
								superuser_emails << admin.email

								UserMailer.welcome_system_email(fullname, email, value, superuser_emails).deliver

								flash["alert"] = "New user, #{fullname} has been created."
								flash["type"] = "success"
							end
						else
							flash["alert"] = "Your session is expired. Please try login again."
							flash["type"] = "warning"

							reset_session
							redirect_to root_path and return
						end
					else
						flash["alert"] = "Your session is expired. Please try login again."
						flash["type"] = "warning"

						reset_session
						redirect_to root_path and return
					end
				else
					reset_session
					redirect_to root_path and return
				end
			rescue
				logger.debug "#{$!}"
				logger.debug "#{$@}"

				flash["alert"] = "Fail to create new user."
				flash["type"] = "warning"

				ActiveRecord::Rollback
			end
		end

		redirect_to users_new_path and return
	end
end
