class Donation < ActiveRecord::Base
	#has_one :currency
	belongs_to :currency
	belongs_to :user
	belongs_to :admin, :class_name => 'User', :foreign_key => 'admin_id'

	def self.to_csv(options = {})
		CSV.generate(options) do |csv|
			csv << column_names
			all.each do |donation|
				csv << donation.attributes.values_at(*column_names)
			end
		end
	end

end
