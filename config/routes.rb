Website::Application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  post 'users/sign_in' => 'users#sign_in'
  post 'users/sign_out' => 'users#sign_out'
  get 'users/dashboard' => 'users#dashboard'
  get 'users/profile/:id' => 'users#profile'
  get 'users/new' => 'users#new'
  get 'donations/new' => 'donations#new'
	get 'donations/edit/:id' => 'donations#edit'
	post 'donations/edit_donation' => 'donations#edit_donation'
  post 'donations/create_donation' => 'donations#create_donation'
	post 'donations/delete' => 'donations#delete'
  post 'users/update_profile' => 'users#update_profile'
  post 'users/create_user' => 'users#create_user'
  get 'users/find_users' => 'users#find_users'
  get 'users/search' => 'users#search'
  get 'donations/search' => 'donations#search'
  get 'donation/export' => 'donations#export'
  get 'users/forgot_password' => 'users#forgot_password'
  get 'send_email' => 'users#send_email'
	post 'users/upload_profile_pic' => 'users#upload_profile_pic'
	get 'users/change_password' => 'users#change_password'
	post 'users/change_password_action' => 'users#change_password_action'
	post 'users/reset_password' => 'users#reset_password'
	get 'request_reset_password' => 'home#request_reset_password'
	post 'users/change_admin' => 'users#change_admin'

	get 'api/authentication_token' => 'api#authentication_token'
	get 'api/donation/:id' => 'api#donation'
	get 'api/user/:id' => 'api#user'
	get	'api/profile/:id' => 'api#profile'
	get	'api/recent/users' => 'api#recent_users'
	get	'api/recent/donations' => 'api#recent_donations'
	get	'api/search/users' => 'api#search_users'
	get	'api/report/users' => 'api#report_users'
	get	'api/report/donations' => 'api#report_donations'
	
	post 'api/login' => 'api#login'
	post 'api/logout' => 'api#logout'
	post 'api/forgot_password' => 'api#forgot_password'
	post 'api/edit/password/:id' => 'api#edit_password'
	post 'api/edit/profile/:id' => 'api#edit_profile'
	post 'api/edit/profile_pic/:id' => 'api#edit_profile_pic'
	post 'api/new/user' => 'api#new_user'
	post 'api/new/donation' => 'api#new_donation'
	post 'api/edit/administratorship/:id' => 'api#edit_administratorship'
	post 'api/edit/donation/:id' => 'api#edit_donation'
	post 'api/delete/donation/:id' => 'api#delete_donation'

	# Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
